(ns frontend.app
  (:require [reagent.core :as reagent :refer [atom]]))

;; Header Component
(defn header []
  [:div#header.clearfix.fill-white
    [:div#header-logo.col.md-col-12.xs-col-12]
    [:div.col.md-col-9.xs-col-0]])


;; Mapbox Map Component
(defn mapbox-map-render []
  [:div#map.xs-full-height.xs-full-width.fill-white])

(defn mapbox-map-did-mount []
  (set! js/mapboxgl.accessToken
    "pk.eyJ1IjoiY29nbml0aXZlcmVmbGV4IiwiYSI6ImNpczUxZWFqcDA5d2EyenAxb3RvdHNmNmgifQ.3svWwUzt3UwvZkjGk_EdcQ")
  (js/console.log js/mapboxgl.accessToken)
  )

(defn mapbox-map []
  (reagent/create-class {:reagent-render mapbox-map-render,
                         :component-did-mount mapbox-map-did-mount}))


;; Content Wrapper Component
(defn content []
  [:div#content.clearfix.fill-grey.xs-col-12
   [mapbox-map]])


;; Calling Component
(defn calling-component []
  [:div.wrapper
   [header]
   [content]])

(defn init []
  (reagent/render-component [calling-component]
                            (.getElementById js/document "container")))
