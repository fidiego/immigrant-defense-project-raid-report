# Raid Map
Part of a larger project, IDP Raid Report

The Raid Map Project aims to provide a visualization of raids the IDP and its affiliates have collected reports on. It aims to fulfill three needs:

1. Provide an easy way to add/edit reports (a CMS of sorts).
2. Provide a UI, light frontend application, to display the information.

## CMS
The CMS functionality is achieved through the use of Google Sheets and App Script.

### App Script
A spreadsheet is loaded with all of the relevant stories and the app script takes care of serializing the data to JSON and sending it via POST to a pseudo-restful service that takes care of storage and retreival.

### Backend
At the moment, the backend API is a pseudo-restful services that handles the storage and retrieval of the json blob it gets from the sheets. It's a dockerized Tornado app deployed to heroku. It is backed by a free redis instance.

## Frontend
The frontend is an old-school `index.html` w/ a raw JS `main.js` file that takes care of all of the logic. We dont' make use of any JS frameworks. It's running with jekyll, but this could easily be replaced. The static site generator used is immaterial, given the size of the project.

We have an experiment with reagent running in [`reagent-frontend`](./reagent-frontend). If we can replicate tthe work there, we may replace the content of [`frontend`](./frontend) with it.


## Augmentations

### Heroku to Lambda
It may behoove us to migrate away from Tornado + Docker on Heroku to Lambda (yay environment variables). If we do, we can remove the redis backend and simply load the blob directly into a bucket. However, we must consider the potential security issues this may raise.
 - what can a malicious actor do if they can POST to our lambda function?
