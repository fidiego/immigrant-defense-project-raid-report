//
// CONSTANTS
//
var IDP_RAID_MAP_API_URL = 'https://idp-raid-map.herokuapp.com'

//
// MENU NOISE
//
function addMenuItems() {
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('Raid Map')
      .addItem('Get Location Data', 'getLocationData')
      .addItem('Upload Data', 'uploadData')
      .addToUi();
}

function onOpen() {
  addMenuItems()
  ping()
}


//
// PING
//
function ping() {
  // ping the backend service on page load to warm up the app
  var response = UrlFetchApp.fetch(IDP_RAID_MAP_API_URL + '/ping');
  Logger.log(response.getContentText())
}


//
// GEOLOCATION NOISE
//
function fetchLocationData(row) {
  if (row == 1) {
    return
  }
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var range = spreadsheet.getRange('D'+ row + ':F' + row);
  var values = range.getValues()[0];
  var state = values[0];
  var county = values[1];
  var zipcode = values[2];

  var queryParameter = '';
  if (zipcode !== '') {
    queryParameter += zipcode;
  } else if (county !== '') {
    queryParameter += county;
    if (state !== '') {
      queryParameter += ',' + state;
    }
  };

  if (queryParameter !== '') {
    var mapAPIUrl = 'https://maps.googleapis.com/maps/api/geocode/json';
    var address = mapAPIUrl + '?address=' + queryParameter;
    var response = UrlFetchApp.fetch(address);
    var body = JSON.parse(response.getContentText());

    var latitude = body.results[0].geometry.location.lat;
    var longitude = body.results[0].geometry.location.lng;

    var latCell = spreadsheet.getRange('G' + row);
    var lonCell = spreadsheet.getRange('H' + row);

    latCell.setValue(latitude);
    lonCell.setValue(longitude);
  };
};

function getLocationData() {
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var cell = spreadsheet.getActiveCell();

  var range = spreadsheet.getActiveRange()
  var firstRow = range.getRow()
  var lastRow = range.getLastRow()

  for (var i = firstRow; i <= lastRow; i++) {
    fetchLocationData(i)
  };
};


//
// UPLOADING THE DATA
//
function makeKey(string) {
  return string.trim().split(' ').join('_').toLowerCase()
}


function serializeData() {
  // get all of the rows and serialize into JSON object for posting
  var spreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = spreadSheet.getSheets()[0];
  var range = sheet.getDataRange();

  // the things we need
  var values = range.getValues();
  var data = values.slice(1, values.length);

  var header = values[0];
  for (var i = 0; i < header.length; i++) {
    header[i] = makeKey(header[i]);
  };

  // convert to json
  var rows = [];
  for (var i = 0; i < data.length; i++) {
    var object = {};
    for (var j = 0; j < data[i].length; j++) {
      if (data[i][j]) {
        object[header[j]] = data[i][j];
      } else {
        object[header[j]] = null;
      }
    }
    rows.push(object)
  };
  return rows;
}

function uploadData() {
  var alert = '';
  var data = serializeData();
  var payload = {
    "raids": data,
    "updated_by": Session.getActiveUser().getEmail()
  }
  var headers = {
    "Accept":"application/json",
    "Content-Type":"application/json",
    "Authorization":"Basic _authcode_"
  };
  var options = {
    "contentType": "application/json",
    "method": "post",
    "headers": headers,
    "payload": JSON.stringify(payload)
  }

  var response = UrlFetchApp.fetch(IDP_RAID_MAP_API_URL + "/api/v1/raids", options);

  if (response.getResponseCode() == 200 ) {
    alert = 'The data was successfully updated. '
    alert += '\nThanks ' + JSON.parse(response.getContentText()).updated_by + '.'
    alert += '\n\nn.b. Your contribution has been logged for posterity.'
  } else {
    alert = 'Something went wrong =(. \nPlease try again. '
  }

  SpreadsheetApp.getUi()
     .alert(alert);
}
