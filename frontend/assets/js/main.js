// Utils
// **********************************
//
var slugifyName = function(name) {
    return name.trim()
        .toLowerCase()
        .split(' ')
        .join('_')
};

var jitter = function(number, factor) {
    // takes a number and adds noise to it
    // factor should be 1, 10, 100, 1000 etc.
    var rnd = Math.random() * factor;
    if (Math.random() > .5) {
        return number + rnd;
    } else {
        return number - rnd;
    }

}

// Data Manipulation Noise
// **********************************
//

var summarizeBoroStats = function(boroName) {
    var _data = data.filter(function(value) {
        return value.county == boroName
    });
    var labels = [];
    var counts = [];

    _data.forEach(function(value, index) {
        if (value.type === '' || !value.type) {
            value.type = 'Unknown'
        }
    });
    _data.forEach(function(value, index) {
        if (!labels.includes(value.type)) {
            labels.push(value.type);
        }
    });
    labels.forEach(function(value, index) {
        _raids = _data.filter(function(r) {
            return r.type == value
        })
        counts.push(_raids.length)
    });
    return [labels, counts]
};

// Graph Helpers
// **********************************
//

var labelColors = {
    'Home Raid': ['rgba(255, 99, 132, 0.2)', 'rgba(255,99,132,1)'],
    'Shelter arrest': ['rgba(54, 162, 235, 0.2)', 'rgba(54, 162, 235, 1)'],
    'Street arrest': ['rgba(255, 206, 86, 0.2)', 'rgba(255, 206, 86, 1)'],
    'Unknown': ['rgba(75, 192, 192, 0.2)', 'rgba(75, 192, 192, 1)'],
    'Workplace arrest': ['rgba(75, 192, 192, 0.2)', 'rgba(75, 192, 192, 1)'],
    'Probation or parole arrest': ['rgba(153, 102, 255, 0.2)', 'rgba(153, 102, 255, 1)'],
    'Courthouse arrest': ['rgba(255, 159, 64, 0.2)', 'rgba(255, 159, 64, 1)']
}

var injectChart = function(boroName) {
    canvas = document.getElementById(slugifyName(boroName) + '-canvas');

    var summary = summarizeBoroStats(boroName);
    labels = summary[0];
    stats = summary[1];

    var colors = [
        [],
        []
    ]
    labels.forEach(function(value, index) {
        var _colors = labelColors[value]
        colors[0].push(_colors[0])
        colors[1].push(_colors[1])
    })

    var barGraph = new Chart(canvas, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                data: stats,
                backgroundColor: colors[0],
                borderColor: colors[1],
                borderWidth: 1,
            }]
        },
        options: {
            responsive: true,
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    return barGraph;
}

// Rendering Stuff
// **********************************
//

var desiredValues = ['type', 'status', 'years_in_us', 'zipcode']
var displayNames = {
    'type': 'Type',
    'status': 'Status',
    'years_in_us': 'Years in US',
    'zipcode': 'Zip Code'
}

function loadBoroStatsModal(boroName){
    var _data = data.filter(function(value) {
        return value.county == boroName
    })
    var raid_count = _data.length;
    document.getElementById('modal-title').innerHTML = boroName;
    document.getElementById('modal-text').innerHTML = '<span class="bold">' + raid_count + '</span>' + ' raids have been reported in ' + boroName;

    document.getElementById('canvas-container').innerHTML = '<canvas id="' + slugifyName(boroName) + '-canvas" class="map-canvas"></canvas>';
    document.getElementById('body').classList.add('js-show-modal')
    injectChart(boroName);
}

function getRaidMarkerPopupContent(raid) {
    var div = document.createElement('div')
    var _innerHTML = '<div class="xs-p1" style="border: 2px solid ' + labelColors[raid.type][1] + '">'

    desiredValues.forEach(function(item, index) {
        if (raid[item] !== '') {
            _innerHTML += '<p><b>' + displayNames[item] + ': </b>' + raid[item] + '</p>'
        }
    });

    _innerHTML += '</div>';
    div.innerHTML = _innerHTML;
    return div;
}

// Data fetching Noise
// ******************************************
//
function loadData(path) {
    var request = new XMLHttpRequest();
    request.open('GET', path, true);
    request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
            data = JSON.parse(request.responseText).results;
            loadMap(data);
            setTimeout(fadeMask, 1000)
            return data;
        } else {
            console.log('error loading data');
        }
    };
    request.onerror = function() {
        console.log('there was an error of sorts');
    };
    request.send();
}

// Map Noise
// ******************************************
// Sets up the map, outline, and event

mapboxgl.accessToken = 'pk.eyJ1IjoiY29nbml0aXZlcmVmbGV4IiwiYSI6ImNpczUxZWFqcDA5d2EyenAxb3RvdHNmNmgifQ.3svWwUzt3UwvZkjGk_EdcQ';

function loadMap(data) {
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v9',
        center: [-73.96, 40.72],
        zoom: 9.75
    });

    map.on('load', function() {
        map.addSource("boroughs", {
            "type": "geojson",
            "data": "/assets/boroughs.geojson"
        });

        map.addLayer({
            "id": "borough-fills",
            "type": "fill",
            "source": "boroughs",
            "layout": {},
            "paint": {
                "fill-color": "#F92600",
                "fill-opacity": 0.15
            }
        });

        map.addLayer({
            "id": "borough-borders",
            "type": "line",
            "source": "boroughs",
            "layout": {},
            "paint": {
                "line-color": "#55496D",
                "line-width": 2
            }
        });

        map.addLayer({
            "id": "route-hover",
            "type": "fill",
            "source": "boroughs",
            "layout": {},
            "paint": {
                "fill-color": "#F92600",
                "fill-opacity": .30
            },
            "filter": ["==", "name", ""]
        });

        map.on("mousemove", function(e) {
            var features = map.queryRenderedFeatures(e.point, {
                layers: ["borough-fills"]
            });
            if (features.length) {
                map.setFilter("route-hover", ["==", "BoroName", features[0].properties.BoroName]);
            } else {
                map.setFilter("route-hover", ["==", "BoroName", ""]);
            }
        });

        // Reset the route-hover layer's filter when the mouse leaves the map
        map.on("mouseout", function() {
            map.setFilter("route-hover", ["==", "BoroName", ""]);
        });


        map.on("click", function(e) {
            var features = map.queryRenderedFeatures(e.point, {
                layers: ['borough-fills']
            });

            if (!features.length) {
                return;
            }

            var feature = features[0];
            var boroName = feature.properties.BoroName;
            loadBoroStatsModal(boroName)
        });

        data.forEach(function(value, index) {
            if (value.lat === null && value.lng === null) {
                return;
            }

            var _div = document.createElement('div');
            _div.style.width = '10px';
            _div.style.height = '10px';
            _div.style.border = '1px solid #000';
            _div.style.backgroundColor = labelColors[value.type][1];
            _div.style.borderRadius = '100%';

            var popup = new mapboxgl.Popup({closeOnClick: true})
                .setDOMContent(getRaidMarkerPopupContent(value))
            var marker = new mapboxgl.Marker(_div)
                .setLngLat([jitter(value.lng, 0.003), jitter(value.lat, 0.003)])
                .setPopup(popup)
                .addTo(map)
        })
    });
}

// fade mask
function fade(element) {
    var op = 1;  // initial opacity
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 50);
}

function fadeMask() {
    var mask = document.getElementsByClassName('mask')[0]
    fade(mask)
}

// Kick off
//  - fetch data
//  - set up map
loadData('https://idp-raid-map.herokuapp.com/api/v1/raids')
