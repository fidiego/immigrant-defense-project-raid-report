# Raid Map

## Functionality
 [X] list all raids in sidebar
 [X] outline of counties
 [ ] add two long island counties
 [ ] summarize data in the popups

## TODO
 - Break out rendering and calculation logic into clearer sections or independent files.
 - Boro Popup Card Rendering: Delete on click off
