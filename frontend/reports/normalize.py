import random
import json
import requests
import sys
import time


county_locations = {
    'Manhattan':  {
        'lat': 40.7830603,
        'lng': -73.9712488
    },
    'Brooklyn': {
        'lat': 40.6781784,
        'lng': -73.9441579
    },
    'Bronx': {
        'lat': 40.8370495,
        'lng': -73.86542949999999
    },
    'Queens': {
        'lat': 40.7282239,
        'lng': -73.7948516
    },
    'Staten Island': {
        'lat': 40.5795317,
        'lng': -74.1502007
    },
    'Newburgh': {
        'lat': 41.5034271,
        'lng': -74.0104178
    },
    'Westchester': {
        'lat': 41.1220194,
        'lng': -73.7948516
    },
    'Wageningen': {
        'lat': 40.7127837,
        'lng': -74.0059413
    },
    'Nassau': {
        'lat': 41.1612401,
        'lng': -71.85620109999999
    },
    'Suffolk': {
        'lat': 40.789142,
        'lng': -73.13496099999999
    }
}


cache = {}


def fetch_location_data(zip_code):
    if isinstance(zip_code, str):
        zip_code = zip_code.strip()
    if cache.get(zip_code):
        return cache.get('zip_code')
    base_url = 'https://maps.googleapis.com/maps/api/geocode/json'
    zip_code = int(zip_code.strip())
    url = '{}?address={}'.format(base_url, zip_code)

    response = requests.get(url)
    if response.ok:
        body = response.json()
        if len(body['results']) < 1:
            print 'No results found for {}'.format(zip_code)
            return None, None
        county = body['results'][0]['address_components'][1]['long_name']
        location = body['results'][0]['geometry']['location']
        cache[zip_code] = (county, location)
        time.sleep(random.uniform(0, 1))
        return county, location


def parse(path_to_file):
    array = []
    with open(path_to_file, 'r') as f:
        for line in f.readlines():
            array.append([c.strip() for c in line.split(',')])
    return array


def main():
    path = sys.argv[1]

    print 'Crunching data at {}'.format(path)

    lines = parse(path)

    header = lines.pop(0)
    header = [heading.lower() for heading in header]

    data = [dict(zip(header, line)) for line in lines]
    omit = []

    for count, datum in enumerate(data, 1):
        print 'Asessing entry {:03d} of {:03d}'.format(count, len(data))
        if datum.get('county') == '' and datum.get('zip_code') == '':
            omit.append(datum)
            print '\tSlated for omission due to missing geolocation data'
            continue

        if datum.get('county') != '' and datum.get('zip_code') == '':
            county = datum.get('county')
            if '(Long Island)' in county:
                datum['county'] = county.split('(')[0].strip()
            county = datum.get('county')
            location = county_locations.get(datum['county'])
            datum['location'] = location
            print 'County only: fetching location from local state - {}'.format(county)
            print '\tfetched location: {}'.format(json.dumps(location))
            continue

        if datum.get('zip_code') != '':
            zip_code = datum.get('zip_code')
            print 'Zip Code: fetching location from google - {}'.format(zip_code)
            county, location = fetch_location_data(zip_code)
            datum['location'] = location
            print '\tset location: {}'.format(json.dumps(location))
            if datum.get('county') == '':
                datum['county'] = county
                print '\tset county: {}'.format(county)
            continue

    for datum in omit:
        data.pop(data.index(datum))

    with open('{}.json'.format(int(time.time())), 'w+') as f:
        f.write(json.dumps(data))
main()
